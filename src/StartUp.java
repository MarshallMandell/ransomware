import java.awt.Color;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class StartUp {

	public void StartUpMain() throws InterruptedException, IOException {
		
		Scanner console_Scanner = new Scanner (System.in);
	    
		System.out.println("Welcome to SNAZZY clock, please enter in some data so that we can get you started!");
		
	   Thread.sleep(500);
		
		Runtime.getRuntime().exec("clear");
		
		System.out.println("What would you like to use? A: StopWatch B: Clock C: Timer?");

		
		String FeatureSU = console_Scanner.next();
		
	
		if (FeatureSU.compareTo("a") == 0){
			
			StopWatch SW = new StopWatch();
			
			SW.StopWatchMain();
    		
		}
		if (FeatureSU.compareTo("A") == 0){
			
			StopWatch SW = new StopWatch();
			
			SW.StopWatchMain();
    		
		}


		
		
	}

}
